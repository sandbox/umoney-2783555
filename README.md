INTRODUCTION
------------
 This is a sandbox project, which contains experimental code for developer use only.
 
 Theme auto module switch theme according url path
 responsive web design is a cheap solution for web page comatible to pc and mobile, however, mobile device become so important, it is worth to design an independent theme for mobile device.
 for example, a typical site may have three theme, one for mobile, one for pc,one for manager, you can configuarate site to change theme following rules
 path /pc/ will switch to desktop theme
 path /mobile/ will switch to mobile theme
 path /console/ will switch to manage theme
 
 mobile device is so important,when detect mobile device, auto switch to mobile theme.. 

INSTALLATION
------------
No special install steps are necessary to use this module, see https://www.drupal.org/documentation/install/modules-themes/modules-8 for further information.

MAINTAINERS
-----------
Current maintainers:

 * Umoney (https://www.drupal.org/u/umoney)
