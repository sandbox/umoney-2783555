<?php

namespace Drupal\theme_auto\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ThemeConfigForm.
 *
 * @package Drupal\cy_theme_auto\Form
 */
class ThemeConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'theme_auto.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'theme_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('theme_auto.settings');
    $settings = '';
    $paths = $config->get('paths');
    foreach ($paths as $theme => $path) {
      $settings .= $theme . '|' . $path . "\n";
    }

    $form['paths'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Auto theme path'),
      '#description' => $this->t('A list of theme and corresponding path,Enter one pair value per line separated by a pipe:
        Examples:
        <ul>
          <li>mobile_theme|/weixin/</li>
          <li>desktop_theme|/pc/</li>
          <li>console_theme|/console/</li>
        </ul>'),
      '#default_value' => $settings,
      '#rows' => 10,
    );
    $form['enable_mobile_switch'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Always switch to mobile theme when detect mobile device '),
      '#description' => $this->t('turn on/off switch to mobile theme'),
      '#default_value' => $config->get('enable_mobile_switch'),
    );
    $form['mobile_theme'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Moble theme'),
      '#description' => $this->t("theme used by mobile device"),
      '#maxlength' => 500,
      '#size' => 64,
      "#default_value" => $config->get('mobile_theme'),
    );
    $form['debug_msg'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Debug:show current url and theme '),
      '#description' => $this->t('turn off in product envirenment'),
      '#default_value' => $config->get('debug_msg')?$config->get('debug_msg'):1,
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $theme_paths = explode("\n",  $form_state->getValue('paths'));
    $settings = array();
    foreach ($theme_paths as $path) {
      $item = explode("|", $path, 2);
      if (count($item) === 2) {
        $settings[$item[0]] = (isset($settings[$item[0]])) ? $settings[$item[0]] . ' ' : '';
        $settings[$item[0]] .= trim($item[1]);
      }
    }
    $config = $this->config('theme_auto.settings');
    $config->set('paths', $settings);
    $config->set('enable_mobile_switch', $form_state->getValue('enable_mobile_switch'));
    $config->set('mobile_theme',$form_state->getValue('mobile_theme'));

    $config->save();
  }

}
