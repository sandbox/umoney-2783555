<?php

/**
 * @file
 * Contains \Drupal\switchtheme\Theme\SwitchthemeNegotiator.
 */

namespace Drupal\theme_auto\Theme;

use Drupal\Core\Theme;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use \Drupal\user\Entity\User;
use Drupal\Core\Controller\ControllerBase;

class SwitchthemeNegotiator extends ControllerBase implements ThemeNegotiatorInterface {

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    $account = \Drupal::currentUser();

    $set_theme = '';
    $query = \Drupal::service('request_stack')->getCurrentRequest();
    $uri = $query->getRequestUri();
    $config = $this->config('theme_auto.settings');
    $paths = $config->get('paths');
    foreach ($paths as $theme => $path) {
      if (strpos($uri, $path) === 0) {
        $set_theme = $theme;
        break;
      }
    }

    if ($this->isMobile() && $config->get('enable_mobile_switch')) {
      $set_theme = $config->get('mobile_theme');
    }
    if($config->get('debug_msg')){
    drupal_set_message('current url ' . $uri . ' and theme ' . $set_theme, 'status', TRUE);
  }
    return $set_theme;
  }

  public function isMobile() {
    if (isset ($_SERVER['HTTP_X_WAP_PROFILE'])) {
      return TRUE;
    }
    if (isset ($_SERVER['HTTP_VIA'])) {
      return stristr($_SERVER['HTTP_VIA'], "wap") ? TRUE : FALSE;
    }

    if (isset ($_SERVER['HTTP_USER_AGENT'])) {
      $clientkeywords = array(
        'nokia',
        'sony',
        'ericsson',
        'mot',
        'samsung',
        'htc',
        'sgh',
        'lg',
        'sharp',
        'sie-',
        'philips',
        'panasonic',
        'alcatel',
        'lenovo',
        'iphone',
        'ipod',
        'blackberry',
        'meizu',
        'android',
        'netfront',
        'symbian',
        'ucweb',
        'windowsce',
        'palm',
        'operamini',
        'operamobi',
        'openwave',
        'nexusone',
        'cldc',
        'midp',
        'wap',
        'mobile'
      );

      if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT']))) {
        return TRUE;
      }
    }

    if (isset ($_SERVER['HTTP_ACCEPT'])) {
      if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== FALSE) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === FALSE || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html')))) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
